from django.db import models

from django.conf import settings
import django.utils.timezone
from django.utils.timezone import activate

import hashlib
import json
import os
from PIL import Image

IDPASSPORT_LENGTH = 50

ICT =  django.utils.timezone.get_fixed_timezone(420)
activate(ICT)

def generate_filename(idname, filename):
    json_str = json.dumps([idname])

    return '{0}.{1}'.format(hashlib.md5(json_str.encode()).hexdigest(), filename.split('.')[-1])

def update_filename(instance, filename):
    path = "users/"
    return '{0}{1}'.format(path, filename)

# Create your models here.
class Member(models.Model):
    id = models.AutoField(primary_key=True)
    idname = models.CharField(max_length=32, blank=True, null=True)
    email = models.CharField(max_length=512, blank=True, null=True)
    password = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to=update_filename, default = 'users/not-img.jpg')
    pub_date = models.DateTimeField('date published', default=django.utils.timezone.now)
    tk = models.CharField(max_length=200, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.image.name = generate_filename(self.idname, self.image.name)
        try:
            image = Image.open(self.image)
            (width, height) = image.size
            is_new_image = width > 120 or height > 120
        except FileNotFoundError:
            is_new_image = False
        if is_new_image:
            try:
                os.remove(os.path.join(settings.MEDIA_ROOT, 'users/{0}'.format(self.image.name)))
            except OSError:
                pass
        super(Member, self).save(*args, **kwargs)
        if is_new_image:
            self.resize_image(image)

    def resize_image(self, image):
            (width, height) = image.size
            if width < height:
                factor = (height / 120) * 100
            else:
                factor = (width / 120) * 100
            new_width = width * 100 / factor
            if new_width < 1:
                new_width = 1
            new_height = height * 100 / factor
            if new_height < 1:
                new_height = 1
            size = (int(new_width), int(new_height))
            image = image.resize(size, Image.ANTIALIAS)
            image.save(self.image.path)

    def __str__(self):
        return str(self.id) + str(self.name)


class Queues(models.Model):

    class STATUS:
        ACTIVE='ACTI'
        DELETE='DELE'
        CHOICES = (
            (ACTIVE, 'Active'),
            (DELETE, 'Delete')
        )

    id = models.AutoField(primary_key=True)
    member_is_queued = models.ForeignKey(Member, related_name='member_is_queued', blank=True, null=True)
    member_queue = models.ForeignKey(Member, related_name='member_queue', blank=True, null=True)
    messages = models.CharField(max_length=200, blank=True, null=True)
    status = models.CharField(max_length=4, choices=STATUS.CHOICES)
    member_throw = models.ForeignKey(Member, blank=True, null=True)
    queue_datetime = models.DateTimeField('datetime queue', default=django.utils.timezone.now)

    def __str__(self):
        return str(self.id) + str(self.member_is_queued)

class Group(models.Model):
    id = models.AutoField(primary_key=True)
    admin_group = models.ForeignKey(Member, related_name='admin_group', blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    password = models.CharField(max_length=4, blank=True, null=True)
    pub_date = models.DateTimeField('date published', default=django.utils.timezone.now)

    def __str__(self):
        return str(self.id) + str(self.name)

class GroupMemer(models.Model):
    class STATUS:
        ADMIN='ADMI'
        MEMBER='MEMB'
        CHOICES = (
            (ADMIN, 'Admin'),
            (MEMBER, 'Member')
        )
    id = models.AutoField(primary_key=True)
    member_group = models.ForeignKey(Member, related_name='member_group', blank=True, null=True)
    group_group = models.ForeignKey(Group, related_name='group_group', blank=True, null=True)
    status = models.CharField(max_length=4, choices=STATUS.CHOICES)
    is_accept = models.BooleanField(default=False)
    pub_date = models.DateTimeField('date published', default=django.utils.timezone.now)

    def __str__(self):
        return str(self.id) + str(self.group_group.name)
