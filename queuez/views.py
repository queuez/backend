from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout, models
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, authentication_classes, parser_classes, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.
from django.http import HttpResponse, JsonResponse


from queuez.models import Member, Queues, Group
from queuez.serializers import MemberSerializer, QueuesDetailSerializer, QueuesSerializer, UpdateQueuesSerializer, ProfileImageSerializer, ProfileNameSerializer, MembersSerializer, SignupSerializer, UserSerializer, GroupsSerializer

from django.views.decorators.csrf import csrf_exempt

import json

class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def index2(request):
    return HttpResponse("Hello, world. You're at the polls index.")

@csrf_exempt
def login(request):
    if request.method in ('POST'):
        data = JSONParser().parse(request)
        user = authenticate(username=data['email'], password=data['password'])
        if user is not None:
            if user.is_active:
                auth_login(request, user)

                member = get_object_or_404(Member, email=data['email'])
                serializer = MemberSerializer(member)
                token = Token.objects.get_or_create(user=user)
                return JsonResponse({'data': serializer.data, 'token': token.__str__()}, status=status.HTTP_200_OK)
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

@csrf_exempt
def logout(request):
    if request.method in ('GET', 'POST'):
        auth_logout(request)
        return HttpResponse(status=status.HTTP_200_OK)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)
"""
@csrf_exempt
def login(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        member = get_object_or_404(Member, un=data["un"], pw=data["pw"])
        serializer = MemberSerializer(member)
        return JsonResponse(serializer.data, status=200)
"""
@csrf_exempt
def signup(request):
    if request.method in ('POST'):
        user_serializer = UserSerializer(data={
            'username': request.POST['email'],
            'password': request.POST['password'],
            'email': request.POST['email']
        })
        if user_serializer.is_valid():
            user_serializer.save()
            signup_serializer = SignupSerializer(data={
                'password': request.POST['password'],
                'email': request.POST['email'],
                'name': request.POST['name'],
                'image': request.FILES['image']
            })
            if signup_serializer.is_valid():
                signup_serializer.save()
                return JsonResponse(signup_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(user_serializer.errors, status=400)
    return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)

class NewMemberDetail(APIView):
    serializer_class = MemberSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class MembersDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = MembersSerializer

    def get(self, request, *args, **kwargs):
        member = Member.objects.filter(~Q(email = request.user.email))
        serializer = self.serializer_class(member, many=True)
        my_member = Member.objects.filter(email = request.user.email)
        my_serializer = self.serializer_class(my_member, many=True)
        return Response({
            "my": my_serializer.data,
            "members": serializer.data
        })

class ProfileNameMember(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileNameSerializer

    def post(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"], email=request.user.email)

        serializer = self.serializer_class(instance=member, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class NewQueues(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = QueuesSerializer

    def post(self, request, *args, **kwargs):
        get_object_or_404(Member, email=request.user.email, id=request.data['member_queue'])
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class ThrowQueues(APIView):
    serializer_class = QueuesSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            queues = get_object_or_404(Queues, idex=kwargs["idex"])
            queues.delete()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class UpdateQueues(APIView):
    serializer_class = UpdateQueuesSerializer
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        queues = get_object_or_404(Queues, id=kwargs["id"], member_queue__email=request.user.email)
        serializer = self.serializer_class(instance=queues, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)

    def put(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        queues = get_object_or_404(Queues, id=kwargs["id"])
        queues.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class QueuesDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = QueuesDetailSerializer

    def get(self, request, *args, **kwargs):
        status = self.request.query_params.get('status', 'A')
        queues = Queues.objects.filter(Q(member_is_queued=kwargs['id']) | Q(member_throw=kwargs['id']) , status=status)
        serializer = self.serializer_class(queues, many=True)
        return Response(serializer.data)

class TakeQueuesDetail(APIView):
    serializer_class = QueuesSerializer

    def get(self, request, *args, **kwargs):
        status = self.request.query_params.get('status', 'A')
        queues = Queues.objects.filter(Q(member_is_queued=kwargs['idex']) | Q(member_throw=kwargs['idex']) , status=status)
        serializer = self.serializer_class(queues, many=True)
        return Response(serializer.data)

class MemberDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = MembersSerializer

    def get(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"])
        serializer = self.serializer_class(member)
        return Response(serializer.data)

class ProfileImageMember(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)
    serializer_class = ProfileImageSerializer

    def post(self, request, *args, **kwargs):
        member = get_object_or_404(Member, idname=kwargs["idname"], email=request.user.email)
        serializer = self.serializer_class(instance=member, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=400)


class GroupsDetail(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = GroupsSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(Group)
        return Response(serializer.data)
