from django.apps import AppConfig


class QueuezConfig(AppConfig):
    name = 'queuez'
