from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^members/$', views.MembersDetail.as_view(), name='members-detail'),
    url(r'^member/(?P<idname>[a-zA-Z0-9_-]+)/$', views.ProfileNameMember.as_view(), name='profile-name-member'),
    url(r'^newqueues/$', views.NewQueues.as_view(), name='new-queues'),
    url(r'^throwqueues/(?P<idex>[a-zA-Z0-9_-]+)/$', views.ThrowQueues.as_view(), name='throw-queues'),
    url(r'^updatequeues/(?P<id>[a-zA-Z0-9_-]+)/$', views.UpdateQueues.as_view(), name='update-queues'),
    url(r'^queuesdetail/(?P<id>[a-zA-Z0-9_-]+)/$', views.QueuesDetail.as_view(), name='my-queues-detail'),
    url(r'^takequeuesdetail/(?P<idex>[a-zA-Z0-9_-]+)/$', views.TakeQueuesDetail.as_view(), name='take-queues-detail'),
    url(r'^memberdetail/(?P<idname>[a-zA-Z0-9_-]+)/$', views.MemberDetail.as_view(), name='member-detail'),
    url(r'^updatephotomember/(?P<idname>[a-zA-Z0-9_-]+)/$', views.ProfileImageMember.as_view(), name='update-photo-member'),
    url(r'^groups/$', views.GroupsDetail.as_view(), name='groups-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=('json',))
