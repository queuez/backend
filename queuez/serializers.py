from rest_framework import serializers
from rest_framework.authtoken.models import Token

from queuez.models import Member, Queues, Group, GroupMemer
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

import hashlib
import json

IDPASSPORT_LENGTH = 50

def generate_idpassport(un):
    data = '{0}{1}'.format(un, get_random_string(IDPASSPORT_LENGTH))
    json_str = json.dumps([data])
    return hashlib.md5(json_str.encode()).hexdigest()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email')

    def create(self, validated_data):
        return User.objects.create_user(validated_data['username'], email=validated_data['email'], password=validated_data['password'])

    def update(self, instance, validated_data):
        instance.password = validated_data.get('password', instance.password)
        instance.save()
        return instance

class MembersSerializer(serializers.ModelSerializer):
    user_count = serializers.SerializerMethodField()
    class Meta:
        model = Member
        fields = ('id', 'idname', 'name', 'image', 'user_count')

    def get_user_count(self, obj):
        return obj.member_is_queued.filter(status='A').count()


class SignupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = ('idname', 'email', 'password', 'name', 'image')

    def create(self, validated_data):
        validated_data['idname'] = generate_idpassport(validated_data['email'])
        return Member.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.password = validated_data.get('password', instance.password)
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance

class ProfileNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('name',)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance

class ProfileImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('image',)

    def update(self, instance, validated_data):
        instance.image = validated_data.get('image', instance.image)
        instance.save()
        return instance

class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'idname', 'email', 'password', 'name', 'image')

class QueuesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Queues
        fields = ('id', 'member_is_queued', 'member_queue', 'messages', 'status', 'member_throw', 'queue_datetime')

    def create(self, validated_data):
        return Queues.objects.create(**validated_data)

class QueuesDetailSerializer(serializers.ModelSerializer):
    member_is_queued_detail = serializers.SerializerMethodField()

    class Meta:
        model = Queues
        fields = ('id', 'messages', 'status', 'member_throw', 'queue_datetime', 'member_is_queued_detail')

    def get_member_is_queued_detail(self, obj):
        return {
            'image': 'media/{0}'.format(obj.member_is_queued.image.name),
            'name': obj.member_is_queued.name,
            'id': obj.member_is_queued.id
        }


class UpdateQueuesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Queues
        fields = ('messages', 'status')

    def update(self, instance, validated_data):
        instance.messages = validated_data.get('messages', instance.messages)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance

class GroupsSerializer(serializers.ModelSerializer):
    user_count = serializers.SerializerMethodField()
    class Meta:
        model = Group
        fields = ('id', 'name', 'user_count')

    def get_user_count(self, obj):
        return obj.group_group.filter(is_accept=True).count()
