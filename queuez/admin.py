from django.contrib import admin

# Register your models here.
from .models import Member, Queues, Group, GroupMemer

admin.site.register(Member)
admin.site.register(Queues)
admin.site.register(Group)
admin.site.register(GroupMemer)
